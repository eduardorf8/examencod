
package creacion;

/**
 *
 * @author erodriguezfernandez
 */

//Lo primero será definir un objeto de tipo Productos.
public class Producto extends Creacion {
    private float precio;
    private String nome;
 
    
    Producto(String nome, float precio){
    	this.nome = nome;
    	this.precio = precio;   
    }
    public String getNome(){
        return nome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public float getPrecio(){
        return precio;
    }
    public void setPrecio(float precio){
        this.precio = precio;
    }

}

