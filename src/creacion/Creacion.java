
package creacion;
import java.util.Map;
import java.util.Iterator;

/**
 *
 * @author erodriguezfernandez
 */
public class Creacion {

    public static void main(String[] args) {
        
        //Lo siguiente que haremos será insertar los objetos Alumnos en un HashSet.

        Producto prod1 = new Producto("Coche",10000);
        Producto prod2 = new Producto("Moto",2000);
        Producto prod3 = new Producto("Kart",3000);
        Producto prod4 = new Producto("Buggy",2500);
        Producto prod5 = new Producto("Quad",1500);
        
        HashSet<Producto> producto = new HashSet<Producto>();
        
        producto.add(prod1);
        producto.add(prod2);
        producto.add(prod3);
        producto.add(prod4);
        producto.add(prod5);
        
        //Si verificamos el tamaño del HashSet productos veremos que nos devuelve "5", ya que por defecto el HashSet no realiza ninguna comparación.
        System.out.println(producto.size());
        
        //Ahora agregamos 5 objetos producto a un HashMap.
        HashMap <Producto,Integer> p = new HashMap<Producto,Integer>();
        
        p.put(prod1, 1);
        p.put(prod2, 2);
        p.put(prod3, 3);
        p.put(prod4, 4);
        p.put(prod5, 5);
        
        //El método .size() nos devolvera un tamaño de "5" ya que el HashMap tampoco realiza ninguna comparación. De igual manerá el método .keySet() devuelve todos los elementos del HashMap.
  
        System.out.println(p.keySet());
  
        System.out.println(p.size());
   
    }
    }
// Commit
//Otro commit mas